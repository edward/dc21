---
title: DebConf21 Online Calls for Proposals is now Open
---

The DebConf Content team would like to call for proposals for the DebConf21
conference, which will take place online from August 22nd to 29th, 2021. We are
interested in traditional presentations and informal dicussion sessions (BoFs),
but we also welcome submissions of tutorials, performances, art installations,
debates, or any other format of event that you think would be of interest to
the Debian community.

For submission information, check out the full [Call for Proposals](/cfp/).
