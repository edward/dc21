---
name: T-Shirt Sizes
---

# T-Shirt sizes

<style type="text/css">
#main table {
  width: auto;
}
</style>
<img src="/static/img/tshirt-size.jpg" style="float: right; max-width: 100%"/>

## Straight cut

| Size | Length in cm (A) | Chest in cm (B)
|------|------------------|----------
| S    | 68               | 50
| M    | 70               | 52
| L    | 71               | 54
| XL   | 74               | 57
| 2XL  | 79               | 60
| 3XL  | 81               | 67
| 4XL  | 84               | 72

## Fitted cut

| Size | Length in cm (A) | Chest in cm (B)
|------|------------------|----------
| S    | 61               | 42
| M    | 64               | 44
| L    | 66               | 47
| XL   | 68               | 50
